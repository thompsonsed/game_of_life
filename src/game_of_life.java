import processing.core.PApplet;

/**
 * Conway's Game of life simulation for processing.
 *
 * Runs the game of life according to the following rules each time step:
 * - Any live cell with fewer than two live neighbours dies.
 * - Any live cell with two or three live neighbours lives on.
 * - Any live cell with more than three live neighbours dies.
 * - Any dead cell with exactly three live neighbours becomes a live cell.
 */
public class game_of_life extends PApplet{
    private static int cellSize = 20;
    private int alive = color(66, 244, 182);
    private int dead = color(50);
    private int lastTime = millis();
    private int interval = 100;
    //
    private int widthSize;
    private int heightSize;
    private int [][] cells;
    private int[][]cellsBuffer;
    // The probability that an individual cell is alive at the start
    private float initial_probablity = 50;
    public void settings()
    {
        noSmooth();
        size(1000, 800);
    }
    public void setup(){
        widthSize = width/cellSize;
        heightSize = height/cellSize;
        cells = new int[widthSize][heightSize];

        cellsBuffer = new int[widthSize][heightSize];
        // This stroke will draw the background grid
        stroke(48);
        for(int x =0; x<widthSize; x++){
            for(int y=0; y<heightSize; y++){
                float state = random(100);
                if(state > initial_probablity){
                    // then the cell is dead
                    cells[x][y] = dead;
                }
                else{
                    cells[x][y] = alive;
                }
            }
        }
        background(50);
    }



    public void draw(){
        for (int x=0; x<widthSize; x++) {
            for (int y=0; y<heightSize; y++) {
                fill(cells[x][y]);
                rect (x*cellSize, y*cellSize, cellSize, cellSize);
            }
        }
        if(millis() - lastTime > interval)
        {
            lastTime = millis();
            iteration();
        }
    }

    /**
     * Performs one step in the game of life.
     */
    public void iteration() {
        // First assign all cells to our buffer
        for(int x = 0; x < widthSize; x ++){
            for(int y = 0; y < heightSize; y++){
                cellsBuffer[x][y] = cells[x][y];
            }
        }
        // Loop over all cells and check for the number of neighbours
        for(int x = 0; x < widthSize; x++){
            for(int y = 0; y < heightSize; y++){
                // Now check all neighbours
                int neighbourAlive = checkNeighbours(x, y);
                // Apply the rules of the game
                if(cellsBuffer[x][y] == alive) {

                    if (neighbourAlive < 2 || neighbourAlive > 3) {
                        cells[x][y] = dead;
                    }
                }
                else{
                    if(neighbourAlive == 3){
                        cells[x][y] = alive;
                    }
                }
            }
        }
    }

    /**
     * Checks the number of alive neighbours for the given x and y coordinates.
     * @param x the x coordinatge to check
     * @param y the y coordinate to check
     * @return the number of alive neighbours
     */
    private int checkNeighbours(int x, int y){
        int neighbourAlive =0;
        for(int x_n = x - 1; x_n < x+2; x_n ++){
            for(int y_n = y - 1; y_n < y + 2; y_n ++){
                if(x_n < 0 || x_n >= widthSize || y_n < 0 || y_n >= heightSize) {
                    continue;
                }
                if(x_n == x && y_n == y)
                {
                    continue;
                }
                if(cellsBuffer[x_n][y_n] == alive) {
                    neighbourAlive ++;
                }
            }
        }
        return neighbourAlive;
    }
    public static void main(String[] passedArgs){
        String[] appletArgs = new String[]{"game_of_life"};
        PApplet.main(appletArgs);
    }
}